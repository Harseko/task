﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Books")]
    public class BooksController : Controller
    {
        private readonly ApplicationContext _context;

        public BooksController(ApplicationContext context)
        {
            _context = context;
            /*Genre genre;
            do
            {
                genre = _context.Genres.FirstOrDefault();
                if (genre != null)
                {
                    _context.Genres.Remove(genre);
                    _context.SaveChanges();
                }
            } while (genre != null);

            Book book;
            do
            {
                book = _context.Books.FirstOrDefault();
                if (book != null)
                {
                    _context.Books.Remove(book);
                    _context.SaveChanges();
                }
            } while (book != null);
            */

            Genre genre1 = new Genre { Name = "Romance" };
            Genre genre2 = new Genre { Name = "Fantasy" };
            Genre genre3 = new Genre { Name = "Horror" };

            Book book1 = new Book { Name = "MyDream", GenreId = genre2.Id, Genre = genre2 };
            Book book2 = new Book { Name = "Lovers", GenreId = genre1.Id, Genre = genre1 };
            Book book3 = new Book { Name = "Pain", GenreId = genre3.Id, Genre = genre3 };


            if (!_context.Genres.Any())
            {
                _context.Genres.AddRange(new List<Genre> { genre1, genre2, genre3 });
                _context.SaveChanges();
            }
            if (!_context.Books.Any())
            {
                _context.Books.AddRange(new List<Book> { book1, book2, book3 });
                _context.SaveChanges();
            }
        }

        // GET: api/Books
        [HttpGet]
        public IEnumerable<Book> GetBooks()
        {
            return _context.Books;
        }

        // GET: api/Books/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBook([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var book = await _context.Books.SingleOrDefaultAsync(m => m.Id == id);

            if (book == null)
            {
                return NotFound();
            }

            return Ok(book);
        }

        // PUT: api/Books/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBook([FromRoute] int id, [FromBody] Book book)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != book.Id)
            {
                return BadRequest();
            }

            _context.Entry(book).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BookExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Books
        [HttpPost]
        public async Task<IActionResult> PostBook([FromBody] Book book)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Books.Add(book);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBook", new { id = book.Id }, book);
        }

        // DELETE: api/Books/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBook([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var book = await _context.Books.SingleOrDefaultAsync(m => m.Id == id);
            if (book == null)
            {
                return NotFound();
            }

            _context.Books.Remove(book);
            await _context.SaveChangesAsync();

            return Ok(book);
        }

        private bool BookExists(int id)
        {
            return _context.Books.Any(e => e.Id == id);
        }
    }
}