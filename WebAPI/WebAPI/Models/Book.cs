﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class Book
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int GenreId { get; set; }
        public Genre Genre { get; set; }
    }

}
