﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Genres")]
    public class GenresController : Controller
    {
        
        private readonly ApplicationContext _context;

        public GenresController(ApplicationContext context)
        {
            _context = context;
            /*Genre genre;
            do
            {
                genre = _context.Genres.FirstOrDefault();
                if (genre != null)
                {
                    _context.Genres.Remove(genre);
                    _context.SaveChanges();
                }
            } while (genre != null);

            Book book;
            do
            {
               book = _context.Books.FirstOrDefault();
                if (book != null)
                {
                    _context.Books.Remove(book);
                    _context.SaveChanges();
                }
            } while (book!=null);
            */

            Genre genre1 = new Genre { Name = "Romance" };
            Genre genre2 = new Genre { Name = "Fantasy" };
            Genre genre3 = new Genre { Name = "Horror" };

            Book book1 = new Book { Name = "MyDream", GenreId = genre2.Id, Genre = genre2 };
            Book book2 = new Book { Name = "Lovers", GenreId = genre1.Id, Genre = genre1 };
            Book book3 = new Book { Name = "Pain", GenreId = genre3.Id, Genre = genre3 };


            if (!_context.Genres.Any())
            {
                _context.Genres.AddRange(new List<Genre> { genre1, genre2, genre3 });
                _context.SaveChanges();
            }
            if (!_context.Books.Any())
            {
                _context.Books.AddRange(new List<Book> { book1, book2, book3 });
                _context.SaveChanges();
            }

        }

        // GET: api/Genres
        [HttpGet]
        public IEnumerable<Genre> GetGenres()
        { 
            return _context.Genres;
        }

        // GET: api/Genres/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetGenre([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var genre = await _context.Genres.SingleOrDefaultAsync(m => m.Id == id);

            if (genre == null)
            {
                return NotFound();
            }

            return Ok(genre);
        }

        // PUT: api/Genres/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGenre([FromRoute] int id, [FromBody] Genre genre)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != genre.Id)
            {
                return BadRequest();
            }

            _context.Entry(genre).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GenreExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Genres
        [HttpPost]
        public async Task<IActionResult> PostGenre([FromBody] Genre genre)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Genres.Add(genre);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetGenre", new { id = genre.Id }, genre);
        }

        // DELETE: api/Genres/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGenre([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var genre = await _context.Genres.SingleOrDefaultAsync(m => m.Id == id);
            if (genre == null)
            {
                return NotFound();
            }

            _context.Genres.Remove(genre);
            await _context.SaveChangesAsync();

            return Ok(genre);
        }

        private bool GenreExists(int id)
        {
            return _context.Genres.Any(e => e.Id == id);
        }
    }
}